import { defineConfig } from 'windicss/helpers'
import colors from 'windicss/colors'

export default defineConfig({
  theme: {
    extend: {
      colors: {
        primary: colors.red,
        secondary: colors.teal,
        background: colors.zinc,
      },
    },
    fontFamily: {
      display: 'DM Sans',
      code: 'Fira Code',
      body: 'Inter',
    },
  },
  plugins: [],
})
