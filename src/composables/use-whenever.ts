import { ref } from 'vue'
import { whenever } from '@vueuse/core'

/**
 * Problem: sometimes content is added to the editor before the editor is done loading, causing errors
 * useWhenever stores callbacks that are run when done() is called
 * callbacks are indexes with a symbol, only the latest callback is called
 */
export function useWhenever() {
  const done = ref(false)

  const cbs: Record<symbol, () => void> = {}

  whenever(done, async() => {
    await Promise.all(Object.values(cbs))
  })

  return {
    done: () => {
      done.value = true
    },
    attempt: (symbol: symbol, cb: () => void) => {
      if (done.value)
        return cb()

      cbs[symbol] = cb
    },
  }
}
