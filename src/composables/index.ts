import { useDark, useToggle } from '@vueuse/core'

export * from './use-fetch'
export * from './use-user'
export * from './use-project'
export * from './use-editor'
export * from './use-whenever'
export * from './use-socket-io'

export const isDark = useDark()
export const toggleDark = useToggle(isDark)
