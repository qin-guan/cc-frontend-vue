import { createFetch } from '@vueuse/core'

export const useFetch = createFetch({
  baseUrl: import.meta.env.VITE_GLOBAL_API_URL,
  fetchOptions: {
    credentials: 'include',
  },
})
