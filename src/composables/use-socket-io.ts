import { onBeforeUnmount, onUnmounted, ref } from 'vue'
import type { Socket } from 'socket.io-client'
import { createEventHook, createSharedComposable } from '@vueuse/core'
import type { Regions } from '../config'

export enum SocketStatus {
  OPEN,
  CONNECTING,
  CLOSED,
}

export enum SocketEvents {
  // User opened the project
  UserJoin = 'userJoin',
  UserJoinRes = 'userJoinRes',
  // User closed the project,
  UserDisconnect = 'userDisconnect',
  // Join room with project
  JoinProject = 'room',
  /**
   * @deprecated
   * Replaced with `Selection`
   * Used for the old cursor implementation for Monaco
   */
  Cursor = 'cursor',
  // CodeMirror selection
  Selection = 'selection',
}

export interface UserJoinData {
  socketID: string
  name: string
  // ID of current file
  currentFile: string
  profilePic: string
}

/**
 * @deprecated
 * Replaced by SelectionData, used for backward compatibility with Monaco editor
 */
export interface CursorData {
  // Socket id
  s: string
  // Just some random string (I think)
  id?: string
  name: string
  file: string
  color: string
  isNew?: boolean
  e: {
    endColumn: number
    endLineNumber: number
    positionColumn: number
    positionLineNumber: number
    selectionStartColumn: number
    selectionStartLineNumber: number
    startColumn: number
    startLineNumber: number
  }
}

export interface SelectionData {
  socketId: string
  // ID of current file
  currentFile: string
  data: {
    from: number
    to: number
  }[]
}

function SocketIO() {
  return import('socket.io-client')
}

export const useSocketIo = createSharedComposable(() => {
  const
    connect = createEventHook<undefined>()
  const userJoin = createEventHook<UserJoinData>()
  // string: socket id of disconnected user
  const userDisconnect = createEventHook<string>()
  const selection = createEventHook<SelectionData>()
  /**
   * @deprecated
   * Replaced by selection
   */
  const cursor = createEventHook<CursorData>()

  const userJoinData = ref<Partial<UserJoinData>>()
  const status = ref(SocketStatus.CONNECTING)

  let socket: Socket | undefined

  function close() {
    socket?.close()
  }

  async function init(region: Regions, isFlask: boolean) {
    if (socket && socket.connected) return

    const { io } = await SocketIO()
    socket = io(import.meta.env.VITE_COMPILER_MASTER_URL.replace('%s', region), {
      path: '/run',
      transports: ['websocket', 'polling'],
      query: { isFlask },
      reconnectionAttempts: 10,
      reconnectionDelay: 300000,
    })

    socket.on('connect', () => {
      status.value = SocketStatus.OPEN
      connect.trigger(undefined)
    })
    socket.on('disconnect', () => status.value = SocketStatus.CLOSED)
    socket.on('connect_error', () => status.value = SocketStatus.CLOSED)

    socket.on(SocketEvents.UserJoinRes, userJoin.trigger)
    socket.on(SocketEvents.UserJoin, (data) => {
      userJoin.trigger(data)
      socket?.emit(SocketEvents.UserJoinRes, userJoinData.value)
    })
    socket.on(SocketEvents.UserDisconnect, userDisconnect.trigger)

    /**
     * cc-compiler-master doesn't forward 'selection' events, so it's sent through 'cursor'
     */
    socket.on(SocketEvents.Cursor, (data) => {
      data.data ? selection.trigger(data) : cursor.trigger(data)
    })
    // socket.on(SocketEvents.Selection, selection.trigger)
  }

  function joinProject(projectId: string, dname: string, currentFile: string, profilePic: string) {
    if (status.value !== SocketStatus.OPEN) {
      console.warn('[useSocketIo] joinSession: socket is not open')
      return
    }

    userJoinData.value = {
      socketID: socket?.id,
      name: dname,
      currentFile,
      profilePic,
    }

    socket?.emit(SocketEvents.JoinProject, projectId)
    socket?.emit(SocketEvents.UserJoin, userJoinData.value)
  }

  /**
   * Emit the cursor
   * @param id: File ID
   * @param data: selection location(s)
   */
  function emitSelection(id: string, data: { from: number; to: number }[]) {
    if (status.value !== SocketStatus.OPEN) {
      console.warn('[useSocketIo] emitSelection: socket is not open')
      return
    }
    /**
     * cc-compiler-master doesn't forward 'selection' events, so it's sent through 'cursor'
     */
    socket?.emit(SocketEvents.Cursor, <SelectionData>{
      socketId: socket?.id,
      currentFile: id,
      data,
    })
  }

  return {
    init,
    close,
    status,
    socket,
    // Actions
    joinProject,
    emitSelection,
    // Events
    onConnect: connect.on,
    onUserJoin: userJoin.on,
    onUserDisconnect: userDisconnect.on,
    onSelection: selection.on,
    /**
     * @deprecated
     * Reaplced by onSelection
     */
    onCursor: cursor.on,
  }
})
