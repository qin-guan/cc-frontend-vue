import { defineStore } from 'pinia'
import { HTTPError } from 'ky'
import { ky } from '../api'

export interface UserInformationDto {
  id: string
  dname: string
  uname: string
  email: string
  profilePic: string
}

export const useUser = defineStore('user', {
  state: (): {
    signedIn: boolean

    id: string
    dname: string
    uname: string
    email: string
    profilePic: string
  } => ({
    signedIn: false,

    id: '',
    dname: '',
    uname: '',
    email: '',
    profilePic: '',
  }),
  actions: {
    /**
     * This should be called on page load
     */
    async whoAmI(): Promise<{ error?: Error }> {
      try {
        const data = await ky
          .post('auth/verifyToken')
          .json<{
          isValid: boolean
          ui?: UserInformationDto
        }>()

        if (!data.isValid) {
          this.$reset()
          return { error: new Error('[useUser] whoAmI: invalid token') }
        }

        this.$patch({
          ...data.ui,
          signedIn: true,
        })

        return {}
      }
      catch (error) {
        if (error instanceof HTTPError) {
          this.$reset()
          return { error: new Error(await error.response.text()) }
        }
        return { error: error as Error }
      }
    },

    /**
     * If no user is signed in, set a guest user.
     * Else do nothing
     */
    loginOptionalGuest() {
      if (this.uname || this.dname) return
      this.uname = 'Guest'
      this.dname = 'Guest'
    },

    /**
     * @param email
     * @param password
     *
     * @return Error message
     */
    async login(email: string, password: string): Promise<{ error?: Error }> {
      try {
        const data = await ky
          .post('auth/login', {
            json: {
              e: email,
              p: password,
            },
          })
          .json<UserInformationDto>()

        this.$patch({
          ...data,
          signedIn: true,
        })

        return {}
      }
      catch (error) {
        if (error instanceof HTTPError)
          return { error: new Error(await error.response.text()) }

        return { error: error as Error }
      }
    },
  },
})
