import { defineStore } from 'pinia'
import { HTTPError } from 'ky'

import { ky } from '../api'
import type { Regions } from '../config'
import { codeMirrorExtMappings } from '../config'
import { useProject } from './use-project'
import { useUser } from './use-user'

export type Languages =
  'cpp'
  | 'css'
  | 'html'
  | 'java'
  | 'javascript'
  | 'json'
  | 'lezer'
  | 'markdown'
  | 'php'
  | 'python'
  | 'rust'
  | 'sql'
  | 'xml'

export interface DetailedProject {
  id: string
  name: string
  mainFile: string
  description: string
  region: Regions
}

export interface FileDto {
  id: string
  name: string
  isFolder: boolean
  parentID: string
}

export interface OpenFile {
  id: string
  name: string
  // CodeMirror language extension name
  cm?: Languages
}

export interface OpenFilePane {
  before: string
  after?: string | OpenFilePane
  // parentId will point to before.id of its parent for all items except the root
  parentId?: string
  reverse: boolean
  vertical: boolean
}

export interface FileOrFolder {
  isFolder: boolean
  name: string
  children: Record<string, FileOrFolder>
}

export const useEditor = defineStore('editor', {
  state: (): {
    // Stores information of project current open in the editor
    project: DetailedProject
    // While dragging, CodeMirror should be hidden so there isn't conflicts with its text dragging features
    dragging: boolean

    openFileIndex: number
    collaborators: Record<string, {
      name: string
      currentFile: string
      profilePic: string
      selections?: Record<string, {
        from: number
        to: number
      }[]>
    }>

    _fileList: FileDto[]
    _openFileIds: string[]

    // WIP: New split editor
    _openFilePaneList: OpenFilePane[]
  } => ({
    project: {
      id: '',
      name: '',
      description: '',
      mainFile: '',
      region: 'US',
    },
    dragging: false,
    openFileIndex: -1,
    collaborators: {},
    _fileList: [],
    _openFileIds: [],

    _openFilePaneList: [],
  }),
  getters: {
    openFilePanes(state) {
      const root: Record<string, OpenFilePane> = {}
      const nodes: Record<string, OpenFilePane> = {}
      for (const pane of state._openFilePaneList) {
        nodes[pane.before] = pane
        if (!pane.parentId)
          root[pane.before] = pane
        else
          nodes[pane.parentId].after = pane
      }
      return root
    },
    collaborator(state) {
      return (socketId: string) => {
        return state.collaborators[socketId]
      }
    },
    // Returns the currently opened file
    currentOpenFile(state): OpenFile {
      return this.openFiles[state.openFileIndex]
    },
    openFiles: (state) => {
      return state._openFileIds
        .map((id) => {
          const file = state._fileList.find(f => f.id === id)
          if (!file) throw new Error('[useEditor] openFiles: item does not exist')
          return file
        })
        .map((f) => {
          const ext = f.name.split('.').pop() ?? ''
          const { cm } = codeMirrorExtMappings.find(l => l.ext.includes(ext)) ?? {}

          return {
            id: f.id,
            name: f.name,
            cm: cm as Languages,
          }
        })
    },
    // Nice tree thingy
    files: (state) => {
      const root: Record<string, FileOrFolder> = {}
      const nodes: Record<string, FileOrFolder> = {}
      for (const {
        id,
        name,
        isFolder,
        parentID,
      } of state._fileList) {
        const item = {
          name,
          isFolder,
          children: {},
        }
        nodes[id] = item

        if (parentID === state.project?.id)
          root[id] = item
        else
          nodes[parentID].children[id] = item
      }
      return root
    },
    filePath(state): (id: string) => string {
      return (id: string) => {
        const file = state._fileList.find(f => f.id === id)
        if (!file) throw new Error('[useEditor] filePath: file does not exist')

        const path: string[] = [file.name]
        let parentId = file.parentID
        while (true) {
          if (parentId === state.project.id) break
          for (const item of state._fileList) {
            if (item.id !== parentId) continue
            path.push(item.name)
            parentId = item.parentID
            break
          }
        }

        return path.reverse().join('/')
      }
    },
  },
  actions: {
    addCollaborator(socketId: string, name: string, currentFile: string, profilePic: string) {
      if (this.collaborators[socketId]) return
      this.collaborators[socketId] = {
        name,
        currentFile,
        profilePic,
      }
    },
    removeCollaborator(socketId: string) {
      delete this.collaborators[socketId]
    },
    closeCurrentProject() {
      this.$reset()
    },
    closeFile(id: string) {
      const idx = this._openFileIds.findIndex(f => f === id)
      if (idx === -1) throw new Error('[useEditor] closeFile: item does not exist')

      this._openFileIds.splice(idx, 1)
      this.openFileIndex = this._openFileIds.length - 1
    },
    openFileV2(id: string, parentId?: string, beforeId?: string) {
      this._openFilePaneList.push({
        parentId,
        before: beforeId ?? id,
        after: beforeId ? id : undefined,
        reverse: false,
        vertical: false,
      })
    },
    openFile(id: string) {
      const idx = this._openFileIds.indexOf(id)
      if (idx > -1) {
        this.openFileIndex = idx
      }
      else {
        this._openFileIds.push(id)
        this.openFileIndex = this.openFiles.length - 1
      }
    },
    openFileByPath(path: string) {
      const split = path.split('/')
      let id = ''
      let parent = this.files
      for (const item of split) {
        const res = Object.keys(parent).find(key => parent[key].name === item)
        if (res) {
          parent = parent[res].children
          id = res
        }
        else {
          id = ''
        }
      }
      if (id.length === 0) return
      this.openFile(id)
    },
    async renameFile(id: string, name: string) {
      if (!this.project) throw new Error('[useEditor] renameFile: project state is empty')

      const user = useUser()
      let originalName = ''

      try {
        // Prioritize faster UI updates
        const idx = this._fileList.findIndex(f => f.id === id)
        if (idx === -1) throw new Error('[useEditor] renameFile: item does not exist')

        originalName = this._fileList[idx].name
        this._fileList[idx].name = name

        await ky
          .post('editor/edit/file', {
            json: {
              fileID: id,
              key: 'name',
              projID: this.project.id,
              uname: user.uname,
              value: name,
            },
          })

        return {}
      }
      catch (error) {
        // If there is error, then undo the changes
        // It's slightly inefficient
        const idx = this._fileList.findIndex(f => f.id === id)
        if (idx === -1) throw new Error('[useEditor] renameFile: item does not exist')

        this._fileList[idx].name = originalName

        if (error instanceof HTTPError) {
          if (error.response.status === 409)
            return { error: new Error('A file/folder with that name already exists') }

          return { error: new Error(await error.response.text()) }
        }
        return { error: error as Error }
      }
    },
    async openProject(projectName: string, ownerName: string): Promise<{ error?: Error }> {
      const project = useProject()
      try {
        const data = await ky
          .post('editor/get/project', {
            json: {
              isWorkspace: project.isCustomWorkspace,
              ownerName,
              projName: projectName,
            },
          })
          .json<{
          id: string
          name: string
          mainFile: string
          description: string
          country: string
        }>()

        this.project = {
          ...data,
          // Should remove country but I'm lazy
          region: data.country as Regions,
        }

        this._fileList = await ky
          .post('editor/get/files', {
            json: {
              projID: data.id,
            },
          }).json<FileDto[]>()

        return {}
      }
      catch (error) {
        if (error instanceof HTTPError) {
          if (error.response.status === 404)
            return { error: new Error('Project not found') }

          return { error: new Error(await error.response.text()) }
        }
        return { error: error as Error }
      }
    },
    async renameCurrentProject(name: string): Promise<{ error?: Error }> {
      const project = useProject()
      const result = await project.renameProject(this.project.id, name)
      if (result.error) return result

      this.project.name = name
      return {}
    },
  },
})
