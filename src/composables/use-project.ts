import { defineStore } from 'pinia'
import { HTTPError } from 'ky'
import { ky } from '../api'
import type { Regions } from '../config'

export interface ProjectDto {
  country: Regions
  lang: number
  name: string
  owner: {
    id: string
    name: string
    email: string
  }
}

export const useProject = defineStore('project', {
  state: (): {
    workspaceId: string
    /**
     * Stores projects the user views in the Projects viewer
     * May be empty (i.e. public projects visited directly by link)
     */
    _projects: {
      '__personal': Record<string, Record<string, ProjectDto>>
      /**
       * {
       *    'workspaceId': {
       *        'path': {
       *          'projectId': ProjectDto
       *        }
       *    }
       * }
       */
      [key: string]: Record<string, Record<string, ProjectDto>>
    }
    /**
     * Same as above for projects, but store folders instead
     */
    _folders: {
      '__personal': Record<string, Record<string, string>>
      // Workspace Id/Name
      [key: string]: Record<string, Record<string, string>>
    }
    /**
     * Same as above for projects, but stores folder metadata instead
     *
     * Mainly just stores the folder ID for each path, because
     * I couldn't figure out a nice way to do it in `_folders`
     */
    _folderMeta: {
      '__personal': Record<string, {
        id: string
      }>
      // Workspace Id/Name
      [key: string]: Record<string, {
        id: string
      }>
    }
  } => ({
    workspaceId: '__personal',
    _projects: {
      __personal: {},
    },
    _folders: {
      __personal: {},
    },
    _folderMeta: {
      __personal: {},
    },
  }),
  getters: {
    isCustomWorkspace: (state) => {
      return state.workspaceId !== '__personal'
    },
    projects: (state) => {
      return (path: string) => state._projects[state.workspaceId][path]
    },
    folders: (state) => {
      return (path: string) => state._folders[state.workspaceId][path]
    },
    folderMeta: (state) => {
      return (path: string) => state._folderMeta[state.workspaceId][path]
    },
  },
  actions: {
    /**
     * Rename a project. If a path is provided, then attempt to
     * update the project value in the store
     * @param id
     * @param name
     * @param path
     */
    async renameProject(id: string, name: string, path?: string): Promise<{ error?: Error }> {
      try {
        await ky
          .post('editor/edit', {
            json: {
              key: 'name',
              projID: id,
              value: name,
            },
          })

        if (!path) return {}
        if (this.projects(path)[id])
          this.projects(path)[id].name = name

        return {}
      }
      catch (error) {
        if (error instanceof HTTPError)
          return { error: new Error(await error.response.text()) }

        return { error: error as Error }
      }
    },
    async createProject(path: string, name: string, description: string, langIdx: number, region: Regions): Promise<{ error?: Error }> {
      try {
        const { id } = this.folderMeta(path)
        if (!id)
          throw new Error('[useProject] createProject: no folderId found')

        await ky
          .post('projs/create', {
            json: {
              name,
              desc: description,
              langNum: langIdx,
              country: region,
              folderID: id,
              // If the workspace is personal, we don't send it
              workspaceId: this.workspaceId === '__personal' ? undefined : this.workspaceId,
            },
          })

        // Fetch the path again.
        // Since we don't have the project Id, we can't update the store :(
        await this.fetch(path)

        return {}
      }
      catch (error) {
        if (error instanceof HTTPError)
          return { error: new Error(await error.response.text()) }

        return { error: error as Error }
      }
    },
    async fetch(path: string): Promise<{ error?: Error }> {
      try {
        const {
          projects,
          folders,
          id,
        } = await ky
          .post('projs/get', {
            json: {
              path,
            },
          })
          .json<{
          folders: Record<string, string>
          id: string
          parent: string
          projects: Record<string, ProjectDto>
        }>()

        this.$state._folders[this.workspaceId][path] = folders
        this.$state._folderMeta[this.workspaceId][path] = { id }

        this.$state._projects[this.workspaceId][path] = projects

        return {}
      }
      catch (error) {
        if (error instanceof HTTPError)
          return { error: new Error(await error.response.text()) }

        return { error: error as Error }
      }
    },
  },
})
