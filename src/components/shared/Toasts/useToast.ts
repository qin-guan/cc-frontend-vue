import { reactive, ref } from 'vue'

export interface ToastOptions {
  title: string
  description?: string
  type: 'default' | 'error' | 'warning' | 'success'
}

const idx = ref(0)
const queue = reactive<Record<string, ToastOptions>>({})

function close(id: string) {
  if (!queue[id]) return
  delete queue[id]
}

export function useToastQueue() {
  return { queue, close }
}

export function useToast() {
  return (options: ToastOptions) => {
    queue[`toast_${idx.value}`] = options
    idx.value++
  }
}
