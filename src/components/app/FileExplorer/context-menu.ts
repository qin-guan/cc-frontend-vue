export interface ContextMenuGroup {
  items: {
    // Emit key when the context item is clicked
    emit: string
    label: string
    textColor?: string
    hoverColor?: string
  }[]
}

export const contextMenuModel: ContextMenuGroup[] = [
  {
    items: [
      {
        emit: 'newFile',
        label: 'New file',
      },
      {
        emit: 'newFolder',
        label: 'New folder',
      },
    ],
  },
  {
    items: [
      {
        emit: 'rename',
        label: 'Rename',
      },
      {
        emit: 'upload',
        label: 'Upload',
      },
    ],
  },
  {
    items: [
      {
        emit: 'info',
        label: 'Info',
      },
      {
        emit: 'delete',
        label: 'Delete',
        textColor: 'text-red-500',
        hoverColor: 'hover:(bg-red-500 text-background-50)',
      },
    ],
  },
]
