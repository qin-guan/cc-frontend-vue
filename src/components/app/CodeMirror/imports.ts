/**
 * Lazy load functions for CodeMirror things to have better code splitting
 */

import type { LanguageSupport } from '@codemirror/language'

type Languages =
  'cpp'
  | 'css'
  | 'html'
  | 'java'
  | 'javascript'
  | 'json'
  | 'lezer'
  | 'markdown'
  | 'php'
  | 'python'
  | 'rust'
  | 'sql'
  | 'xml'

export function Prettier() {
  return import('prettier')
}

export async function PrettierLanguagePlugin(lang: Languages) {
  switch (lang) {
    case 'css':
      return [await import('prettier/parser-postcss')]
    case 'html':
      return [await import('prettier/parser-html')]
    case 'json':
    case 'javascript':
      return [await import('prettier/parser-babel'), await import('prettier/parser-typescript'), await import('prettier/parser-flow')]
    case 'markdown':
      return [await import('prettier/parser-markdown')]
    default:
      return []
  }
}

export function CMBasicSetup() {
  return import('./basicSetup')
}

export function CMCommands() {
  return import('@codemirror/commands')
}

export function CMView() {
  return import('@codemirror/view')
}

export function CMCollab() {
  return import('@codemirror/collab')
}

export function CMState() {
  return import('@codemirror/state')
}

export function CMTheme() {
  return import('./themes')
}

export async function CMCompartment() {
  return (await import('@codemirror/state')).Compartment
}

export async function CMLang(lang?: Languages): Promise<LanguageSupport> {
  switch (lang) {
    case 'cpp':
      return (await import('@codemirror/lang-cpp')).cpp()
    case 'css':
      return (await import('@codemirror/lang-css')).css()
    case 'html':
      return (await import('@codemirror/lang-html')).html({ autoCloseTags: true })
    case 'java':
      return (await import('@codemirror/lang-java')).java()
    case 'javascript':
      return (await import('@codemirror/lang-javascript')).javascript({ typescript: true })
    case 'json':
      return (await import('@codemirror/lang-json')).json()
    case 'lezer':
      return (await import('@codemirror/lang-lezer')).lezer()
    case 'php':
      return (await import('@codemirror/lang-php')).php()
    case 'python':
      return (await import('@codemirror/lang-python')).python()
    case 'rust':
      return (await import('@codemirror/lang-rust')).rust()
    case 'sql':
      return (await import('@codemirror/lang-sql')).sql()
    case 'xml':
      return (await import('@codemirror/lang-xml')).xml()
    // A compartment for language must be set, so by default I'll just set MD
    case 'markdown':
    default:
      return (await import('@codemirror/lang-markdown')).markdown()
  }
}
