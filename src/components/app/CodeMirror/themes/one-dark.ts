import type { Extension } from '@codemirror/state'

import { EditorView } from '@codemirror/view'
import { HighlightStyle, tags as t } from '@codemirror/highlight'

import colors from 'windicss/colors'

const chalky = colors.yellow[400]
const coral = colors.pink[400]
const cyan = colors.cyan[400]
const invalid = colors.white
const ivory = colors.gray[300]
const stone = colors.gray[500]
const malibu = colors.sky[400]
const sage = colors.emerald[400]
const whiskey = colors.amber[400]
const violet = colors.violet[400]
const background = colors.zinc[900]
const darkBackground = colors.zinc[900]
const highlightBackground = colors.zinc[900]
const tooltipBackground = colors.zinc[600]
const selection = colors.zinc[700]
const cursor = colors.zinc[100]

const sharedThemeSpec = {
  '&': {
    color: ivory,
    backgroundColor: background,
    width: '100%',
    height: '100%',
  },

  '.cm-content': {
    caretColor: cursor,
    fontFamily: 'Fira Code',
    fontSize: '15px',
  },

  '.cm-cursor, .cm-dropCursor': { borderLeftColor: cursor },
  '&.cm-focused .cm-selectionBackground, .cm-selectionBackground, .cm-content ::selection': { backgroundColor: selection },

  '.cm-panels': {
    backgroundColor: darkBackground,
    color: ivory,
  },
  '.cm-panels.cm-panels-top': { borderBottom: '2px solid black' },
  '.cm-panels.cm-panels-bottom': { borderTop: '2px solid black' },

  '.cm-searchMatch': {
    backgroundColor: '#72a1ff59',
    outline: '1px solid #457dff',
  },
  '.cm-searchMatch.cm-searchMatch-selected': {
    backgroundColor: '#6199ff2f',
  },

  '.cm-activeLine': { backgroundColor: highlightBackground },
  '.cm-selectionMatch': { backgroundColor: '#aafe661a' },

  '&.cm-focused .cm-matchingBracket, &.cm-focused .cm-nonmatchingBracket': {
    backgroundColor: '#bad0f847',
    outline: '1px solid #515a6b',
  },

  '.cm-gutters': {
    backgroundColor: background,
    color: stone,
    border: 'none',
  },

  '.cm-activeLineGutter': {
    backgroundColor: highlightBackground,
  },

  '.cm-foldPlaceholder': {
    backgroundColor: 'transparent',
    border: 'none',
    color: '#ddd',
  },

  '.cm-tooltip': {
    border: 'none',
    backgroundColor: tooltipBackground,
  },
  '.cm-tooltip .cm-tooltip-arrow:before': {
    borderTopColor: 'transparent',
    borderBottomColor: 'transparent',
  },
  '.cm-tooltip .cm-tooltip-arrow:after': {
    borderTopColor: tooltipBackground,
    borderBottomColor: tooltipBackground,
  },
  '.cm-tooltip-autocomplete': {
    '& > ul > li[aria-selected]': {
      backgroundColor: highlightBackground,
      color: ivory,
    },
  },
}

export const oneDarkTheme = EditorView.theme({
  '&': {
    color: ivory,
    backgroundColor: background,
    width: '100%',
    height: '100%',
  },

  '.cm-content': {
    'caretColor': cursor,
    'font-family': 'Fira Code',
    'font-size': '15px',
  },

  '.cm-cursor, .cm-dropCursor': { borderLeftColor: cursor },
  '&.cm-focused .cm-selectionBackground, .cm-selectionBackground, .cm-content ::selection': { backgroundColor: selection },

  '.cm-panels': {
    backgroundColor: darkBackground,
    color: ivory,
  },
  '.cm-panels.cm-panels-top': { borderBottom: '2px solid black' },
  '.cm-panels.cm-panels-bottom': { borderTop: '2px solid black' },

  '.cm-searchMatch': {
    backgroundColor: '#72a1ff59',
    outline: '1px solid #457dff',
  },
  '.cm-searchMatch.cm-searchMatch-selected': {
    backgroundColor: '#6199ff2f',
  },

  '.cm-activeLine': { backgroundColor: highlightBackground },
  '.cm-selectionMatch': { backgroundColor: '#aafe661a' },

  '&.cm-focused .cm-matchingBracket, &.cm-focused .cm-nonmatchingBracket': {
    backgroundColor: '#bad0f847',
    outline: '1px solid #515a6b',
  },

  '.cm-gutters': {
    backgroundColor: background,
    color: stone,
    border: 'none',
  },

  '.cm-activeLineGutter': {
    backgroundColor: highlightBackground,
  },

  '.cm-foldPlaceholder': {
    backgroundColor: 'transparent',
    border: 'none',
    color: '#ddd',
  },

  '.cm-tooltip': {
    border: 'none',
    backgroundColor: tooltipBackground,
  },
  '.cm-tooltip .cm-tooltip-arrow:before': {
    borderTopColor: 'transparent',
    borderBottomColor: 'transparent',
  },
  '.cm-tooltip .cm-tooltip-arrow:after': {
    borderTopColor: tooltipBackground,
    borderBottomColor: tooltipBackground,
  },
  '.cm-tooltip-autocomplete': {
    '& > ul > li[aria-selected]': {
      backgroundColor: highlightBackground,
      color: ivory,
    },
  },
}, { dark: true })

/// The highlighting style for code in the One Dark theme.
export const oneDarkHighlightStyle = HighlightStyle.define([
  {
    tag: t.keyword,
    color: violet,
  },
  {
    tag: [t.name, t.deleted, t.character, t.propertyName, t.macroName],
    color: coral,
  },
  {
    tag: [t.function(t.variableName), t.labelName],
    color: malibu,
  },
  {
    tag: [t.color, t.constant(t.name), t.standard(t.name)],
    color: whiskey,
  },
  {
    tag: [t.definition(t.name), t.separator],
    color: ivory,
  },
  {
    tag: [t.typeName, t.className, t.number, t.changed, t.annotation, t.modifier, t.self, t.namespace],
    color: chalky,
  },
  {
    tag: [t.operator, t.operatorKeyword, t.url, t.escape, t.regexp, t.link, t.special(t.string)],
    color: cyan,
  },
  {
    tag: [t.meta, t.comment],
    color: stone,
  },
  {
    tag: t.strong,
    fontWeight: 'bold',
  },
  {
    tag: t.emphasis,
    fontStyle: 'italic',
  },
  {
    tag: t.strikethrough,
    textDecoration: 'line-through',
  },
  {
    tag: t.link,
    color: stone,
    textDecoration: 'underline',
  },
  {
    tag: t.heading,
    fontWeight: 'bold',
    color: coral,
  },
  {
    tag: [t.atom, t.bool, t.special(t.variableName)],
    color: whiskey,
  },
  {
    tag: [t.processingInstruction, t.string, t.inserted],
    color: sage,
  },
  {
    tag: t.invalid,
    color: invalid,
  },
])

/// Extension to enable the One Dark theme (both the editor theme and
/// the highlight style).
export const oneDark: Extension = [oneDarkTheme, oneDarkHighlightStyle]
