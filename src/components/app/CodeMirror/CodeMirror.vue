<script setup lang="ts">
import { nextTick, onBeforeUnmount, onMounted, reactive, ref, watch } from 'vue'
import { HTTPError } from 'ky'
import type { EditorView } from '@codemirror/view'
import type { ChangeSpec, Compartment, EditorState } from '@codemirror/state'
import ReconnectingWebSocket from 'reconnecting-websocket'
import type { Doc } from 'sharedb/lib/client'
import { Connection } from 'sharedb/lib/client'
import { Error, FlexScroll, useToast } from '../../shared'
import type { Languages } from '../../../composables'
import type { Regions } from '../../../config'
import { isDark, useEditor, useSocketIo, useUser, useWhenever } from '../../../composables'
import { regionalKy } from '../../../api'
import {
  CMBasicSetup,
  CMCommands,
  CMCompartment,
  CMLang,
  CMState,
  CMTheme,
  CMView,
  Prettier,
  PrettierLanguagePlugin,
} from './imports'

/**
 * Although the component has access to Pinia and could theoretically handle all
 * data with Pinia directly (which were how things were done previously),
 * it is better to have file specific data passed as props so the component can be
 * reused easily (i.e. split editors).
 *
 * In the case of multiple editors, the ShareDB connection can be reused between components
 */
const props = defineProps<{
  connection?: Connection
  file?: {
    id: string
    name: string
    cm: Languages
  }
  project?: {
    id: string
    name: string
    region: Regions
  }
}>()

const toast = useToast()
const socketIo = useSocketIo()

const user = useUser()
const editor = useEditor()
const {
  done,
  attempt,
} = useWhenever()

const editorSetInitial = Symbol('editorSetInitial')
const editorSetLanguage = Symbol('editorSetLanguage')

const container = ref<HTMLDivElement>()
const state = reactive({
  loading: true,
  // Min: 0, Max: 10
  progressBar: 1,
  error: '',
})
const editorCache: Record<string, {
  /**
   * @deprecated Should use doc.data.data instead
   */
  data: string
  doc: Doc
}> = {}

const LOADING_TEXT = '✨ Loading... ✨'

if (!editor.project) throw new Error('[CodeMirror] project is undefined')

let currentVersion = 0
let suppress = true
let editorView: EditorView
let editorTheme: Compartment
let editorLanguage: Compartment
let editorEditable: Compartment

const socket = new ReconnectingWebSocket(`${import.meta.env.VITE_SHARE_URL}/sh?r=${!!user.uname}&u=${user.uname}&id=${user.id}`.replace('%s', editor.project.region.toLowerCase()))
// @ts-expect-error ShareDB expects their own Socket interface but ReconnectingWebSocket works fine as well
const connection = new Connection(socket)

// socketIo.onSelection(async({ data, currentFile, socketId }) => {
//   const user = editor.collaborator(socketId)
//   if (!user) return
// })

function dispatchSuppressed(changes: ChangeSpec) {
  const txn = editorView.state.update({ changes })
  suppress = true
  editorView.dispatch(txn)
  suppress = false
}

async function formatCode(ev: EditorView) {
  if (!editor.currentOpenFile.cm) return

  const prettier = await Prettier()
  const plugins = await PrettierLanguagePlugin(editor.currentOpenFile.cm)
  if (!plugins) return

  try {
    const code = prettier.format(ev.state.doc.toJSON().join('\n'), {
      filepath: editor.currentOpenFile.name,
      plugins: [...plugins],
      semi: true,
    })
    const txn = editorView.state.update({
      changes: {
        from: 0,
        to: ev.state.doc.length,
        insert: code,
      },
    })
    editorView.dispatch(txn)
  }
  catch (e) {
    toast({
      title: 'Format',
      description: (e as Error).message,
      type: 'error',
    })
  }
}

onMounted(async() => {
  // Lazy load CodeMirror
  const cmLang = await CMLang(editor.currentOpenFile.cm)
  state.progressBar = 2
  const { indentWithTab } = await CMCommands()
  state.progressBar = 3
  const { EditorState, Text } = await CMState()
  state.progressBar = 4
  const { EditorView, keymap } = await CMView()
  state.progressBar = 5
  const Compartment = await CMCompartment()
  editorTheme = new Compartment()
  editorLanguage = new Compartment()
  editorEditable = new Compartment()
  state.progressBar = 6
  const {
    oneDark,
    oneLight,
  } = await CMTheme()
  state.progressBar = 8
  const { basicSetup } = await CMBasicSetup()
  state.progressBar = 10
  state.loading = false

  await nextTick(() => {
    if (!container.value) return

    const extensions = [
      basicSetup,
      keymap.of([
        {
          key: 'Mod-s',
          preventDefault: true,
          run: (ev) => {
            formatCode(ev)
            return true
          },
        },
        indentWithTab,
      ]),
      editorTheme.of(isDark.value ? oneDark : oneLight),
      editorEditable.of(EditorView.editable.of(false)),
      EditorView.updateListener.of((update) => {
        if (update.focusChanged || update.docChanged || update.heightChanged || update.geometryChanged || update.viewportChanged) return

        socketIo.emitSelection(editor.currentOpenFile.id, update.state.selection.ranges.map(({
          from,
          to,
        }) => ({
          from,
          to,
        })))
      }),
      EditorView.updateListener.of((update) => {
        if (!update.docChanged || suppress || !editorCache[editor.currentOpenFile.id]) return

        const
          ops: { p: [string, number]; sd?: string; si?: string }[] = []
        const data = editorCache[editor.currentOpenFile.id].doc.data.data
        const newline = data.includes('\r\n') ? '\r\n' : '\n'

        update.changes.iterChanges((fromA, toA, fromB, toB, inserted) => {
          const
            insertedText = inserted.toJSON().join(newline)
          const startSlice = update.startState.doc.slice(fromA, toA)

          // Avoid extra calculations if only one line was changed
          if (startSlice.lines === 1) {
            if (fromA < toB && toA > fromB) { // Replace
              ops.push(
                {
                  p: ['data', fromB],
                  sd: startSlice.toJSON().join(''),
                },
                {
                  p: ['data', fromB],
                  si: insertedText,
                },
              )
            }
            else if (toA > fromA) { // Delete
              ops.push({
                p: ['data', fromB],
                sd: startSlice.toJSON().join(''),
              })
            }
            else { // Insert
              ops.push({
                p: ['data', fromB],
                si: insertedText,
              })
            }
          }
          else {
            /**
             * Transform CodeMirror position to json0 positions
             * Calculate number of lines and compensate missing newline chars length
             * @param pos
             */
            function transformPos(pos: number) {
              const compensation = (update.startState.doc.slice(0, pos).lines - 1) * (newline.length - 1)
              return pos + compensation
            }

            if (fromA < toB && toA > fromB) { // Replace
              ops.push(
                {
                  p: ['data', transformPos(fromB)],
                  sd: update.startState.doc.toJSON().join(newline).substring(transformPos(fromA), transformPos(toA)),
                },
                {
                  p: ['data', transformPos(fromB)],
                  si: insertedText,
                },
              )
            }
            else if (toA > fromA) { // Delete
              ops.push({
                p: ['data', transformPos(fromB)],
                sd: update.startState.doc.toJSON().join(newline).substring(transformPos(fromA), transformPos(toA)),
              })
            }
            else { // Insert
              ops.push({
                p: ['data', transformPos(fromB)],
                si: insertedText,
              })
            }
          }
        })

        const thisVersion = currentVersion
        currentVersion++
        editorCache[editor.currentOpenFile.id].doc.submitOp(ops, { source: true }, (e) => {
          if (e) throw e
          // If version when op was sent is different from the current version, ignore
          if (thisVersion !== currentVersion) return
          // If content matches, return
          if (editorCache[editor.currentOpenFile.id].doc.data.data === editorView.state.doc.toJSON().join(newline)) return
          // If content doesn't match, something must've exploded along the way, reset the editor
          console.warn(
            `[CodeMirror] submitOp callback:\n\tCurrent version: ${currentVersion}\n\tOld: ${editorCache[editor.currentOpenFile.id].doc.data.data}\n\tNew: ${editorView.state.doc.toJSON().join(newline)}`,
          )
          attempt(Symbol('fix'), () => {
            dispatchSuppressed({
              from: 0,
              to: editorView.state.doc.length,
              insert: editorCache[editor.currentOpenFile.id].doc.data.data,
            })
          })
        })
      }),
    ]

    if (cmLang) extensions.push(editorLanguage.of(cmLang))

    editorView = new EditorView({
      state: EditorState.create({
        extensions,
        doc: Text.of([LOADING_TEXT]),
      }),
      parent: container.value,
    })

    done()
  })
})

watch(() => editor.currentOpenFile, async(value, _, onCleanup) => {
  if (!value) return

  if (state.error) state.error = ''
  if (!editor.project) {
    console.warn('[CodeMirror] watch(editor.currentOpenFile): project is undefined, will not continue')
    return
  }

  attempt(editorSetLanguage, async() => {
    const cmLang = await CMLang(value.cm)
    if (!cmLang) return

    editorView.dispatch({
      effects: editorLanguage.reconfigure(cmLang),
    })
  })

  const { EditorView } = await CMView()
  if (!editorCache[value.id]) { // Attempt to set "Loading..." to editor
    attempt(editorSetInitial, () => {
      editorView.dispatch({
        effects: editorEditable.reconfigure(EditorView.editable.of(false)),
      })
      dispatchSuppressed({
        from: 0,
        to: editorView.state.doc.length,
        insert: LOADING_TEXT,
      })
    })
  }
  else { // Return cached version first, before updating from server
    attempt(editorSetInitial, async() => {
      dispatchSuppressed({
        from: 0,
        to: editorView.state.doc.length,
        insert: editorCache[value.id].doc.data.data,
      })
      editorView.dispatch({
        effects: editorEditable.reconfigure(EditorView.editable.of(true)),
      })
    })
  }

  try {
    await regionalKy(editor.project.region).post('editor/share/init', {
      json: {
        projID: editor.project.id,
        fileID: value.id,
        uname: user.uname,
        country: editor.project.region,
      },
    })

    if (!editorCache[value.id]) {
      editorCache[value.id] = {
        doc: connection.get('sharedb', value.id),
        data: '',
      }
      editorCache[value.id].doc.submitSource = true

      /**
       * Transform position in string to CodeMirror ChangeSpec positions
       *
       * Newlines are considered 1 char in CodeMirror, but could be 2 (\n) or 4 (\r\n) chars
       * @param pos
       */
      function transformPos(pos: number) {
        const str = editorCache[value.id].doc.data.data.substring(0, pos)
        return transformLength(str)
      }

      function transformLength(str: string) {
        return str.replaceAll(/\r?\n/g, 'n').length
      }

      editorCache[value.id].doc.on<'op'>('op', async(ops, source) => {
        if (source) return

        ops.forEach((op) => {
          const {
            p,
            si,
            sd,
          } = op
          const pos = p[p.length - 1]

          let changes: ChangeSpec

          if (sd) { // Delete
            changes = {
              from: transformPos(pos),
              to: transformPos(pos) + transformLength(sd),
              insert: '',
            }
          }
          if (si) { // Insert
            changes = {
              from: transformPos(pos),
              to: transformPos(pos),
              insert: si,
            }
          }

          editor.currentOpenFile.id === value.id && attempt(editorSetInitial, () => {
            dispatchSuppressed(changes)
          })
        })
      })
    }

    editorCache[value.id].doc.subscribe((err) => {
      if (err) throw new Error(`[CodeMirror] ${err.message}`)
      if (!editorCache[value.id].doc.type)
        throw new Error('[CodeMirror] ShareDB document uninitialized. Check if the id is correct and you have initialised the document on the server.')

      editor.currentOpenFile.id === value.id && attempt(editorSetInitial, () => {
        dispatchSuppressed({
          from: 0,
          to: editorView.state.doc.length,
          insert: editorCache[value.id].doc.data.data,
        })
        editorView.dispatch({
          effects: editorEditable.reconfigure(EditorView.editable.of(true)),
        })
      })
    })
  }
  catch (error) {
    console.error(error)
    if (error instanceof HTTPError)
      state.error = await error.response.text()
    else
      state.error = (error as Error).message
  }

  onCleanup(() => {
    editorCache[value.id].doc.unsubscribe()
  })
}, {
  immediate: true,
})

watch(isDark, async() => {
  if (!editorView) return
  const {
    oneDark,
    oneLight,
  } = await CMTheme()

  editorView.dispatch({
    effects: editorTheme.reconfigure(isDark.value ? oneDark : oneLight),
  })
})

onBeforeUnmount(() => {
  editorView?.destroy()
  socket?.close()
})
</script>

<template>
  <div v-if="state.loading">
    <!--I spent too much time on this-->
    <span class="text-base font-semibold font-code text-background-900 dark:text-background-400">
      {{
        '\uee03' + '\uee04'.repeat(state.progressBar * 2) + '\uee01'.repeat(20 - (state.progressBar * 2)) + (state.progressBar === 10 ? '\uee05' : '\uee02')
      }}
      {{ state.progressBar * 10 }}%
    </span>
  </div>

  <Error v-else-if="state.error" :title="state.error" />

  <FlexScroll v-else vertical horizontal>
    <div v-show="!editor.dragging" ref="container" class="flex-1" />
  </FlexScroll>
</template>
