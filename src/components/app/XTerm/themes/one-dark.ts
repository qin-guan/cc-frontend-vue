import type { ITheme } from 'xterm'
import colors from 'windicss/colors'

export const oneDark: ITheme = {
  background: colors.zinc[800],
  black: colors.black[900],
  blue: colors.blue[400],
}
