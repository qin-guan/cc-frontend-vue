/// <reference types="vite/client" />

declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
  const component: DefineComponent<{}, {}, any>
  export default component
}

interface ImportMetaEnv {
  readonly VITE_GLOBAL_API_URL: string
  readonly VITE_REGIONAL_API_URL: string
  readonly VITE_SHARE_URL: string
  readonly VITE_COMPILER_MASTER_URL: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
