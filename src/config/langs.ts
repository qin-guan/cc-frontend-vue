export const langs = [
  {
    name: 'Python',
    version: '3.9',
    ext: ['py'],
    img: 'python.svg',
  },
  {
    name: 'NodeJS',
    version: '10.24.1',
    ext: ['js'],
    img: 'nodejs.svg',
  },
  {
    name: 'CoffeeScript',
    version: '2.5.1',
    ext: ['coffee'],
    img: 'coffeescript.svg',
  },
  {
    name: 'Typescript',
    version: '4.1.3',
    ext: ['ts'],
    img: 'typescript.svg',
  },
  {
    name: 'C++',
    version: '17',
    ext: ['cpp', 'h'],
    img: 'cpp.svg',
  },
  {
    name: 'C',
    version: '',
    ext: ['c', 'h'],
    img: 'c.svg',
  },
  {
    name: 'C#',
    version: '6.12.0.107',
    ext: ['cs'],
    img: 'csharp.svg',
  },
  {
    name: 'HTML, CSS & JS',
    version: '',
    ext: ['html', 'css', 'js'],
    img: 'html.svg',
  },
  {
    name: 'Python',
    version: '2.7',
    ext: ['py'],
    img: 'python.svg',
  },
  {
    name: 'Bash',
    version: '5.0.17(1)',
    ext: ['sh'],
    img: 'unknown.svg',
  },
  {
    name: 'Java',
    version: '11.0.9',
    ext: ['java'],
    img: 'java.svg',
  },
  {
    name: 'Swift',
    version: '5.3.1',
    ext: ['swift'],
    img: 'swift.svg',
  },
  {
    name: 'Golang',
    version: '1.13.8',
    ext: ['go'],
    img: 'golang.svg',
  },
  {
    name: 'PHP',
    version: '7.4.3',
    ext: ['php'],
    img: 'php.svg',
  },
  {
    name: 'Ruby',
    version: '2.7.0',
    ext: ['rb'],
    img: 'ruby.svg',
  },
  {
    name: 'Perl',
    version: '5.30.0',
    ext: ['pl'],
    img: 'perl.svg',
  },
  {
    name: 'Kotlin (BETA)',
    version: '1.4.21',
    ext: ['kt'],
    img: 'kotlin.svg',
  },
  {
    name: 'Unused Compiler',
    version: '',
    ext: [''],
    img: 'unknown.svg',
  },
  {
    name: 'Unused Compiler',
    version: '',
    ext: [''],
    img: 'unknown.svg',
  },
  {
    name: 'Python Turtle',
    version: '',
    ext: ['py'],
    img: 'python.svg',
  },
  {
    name: 'Python Flask (BETA)',
    version: '',
    ext: ['py'],
    img: 'python.svg',
  },
  {
    name: 'Racket',
    version: '8.1',
    ext: ['rkt'],
    img: 'racket.svg',
  },
]

export const codeMirrorExtMappings = [
  {
    ext: ['cpp', 'c', 'h'],
    cm: 'cpp',
  },
  {
    ext: ['css', 'sass', 'scss', 'less'],
    cm: 'css',
  },
  {
    ext: ['html', 'xhtml', 'razor'],
    cm: 'html',
  },
  {
    ext: ['java'],
    cm: 'java',
  },
  {
    ext: ['js', 'ts', 'jsx', 'tsx'],
    cm: 'javascript',
  },
  {
    ext: ['json'],
    cm: 'json',
  },
  {
    ext: ['md', 'mdx'],
    cm: 'markdown',
  },
  {
    ext: ['php'],
    cm: 'php',
  },
  {
    ext: ['py'],
    cm: 'python',
  },
  {
    ext: ['rs'],
    cm: 'rust',
  },
  {
    ext: ['sql'],
    cm: 'sql',
  },
  {
    ext: ['xml'],
    cm: 'xml',
  },
]
