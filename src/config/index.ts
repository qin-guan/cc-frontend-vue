export * from './langs'

export type Regions = 'US' | 'EU' | 'SG'
export const regions: Regions[] = ['US', 'EU', 'SG']
