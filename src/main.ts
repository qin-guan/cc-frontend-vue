import { createApp } from 'vue'

import { createRouter, createWebHistory } from 'vue-router'
import { createPinia } from 'pinia'

import 'virtual:windi.css'
import 'virtual:windi-devtools'

import './app.css'

import App from './App.vue'

import About from './pages/landing/About.vue'
import Login from './pages/landing/Login.vue'
import NotFound from './pages/landing/NotFound.vue'

import Home from './pages/app/Home.vue'

const routes = [
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    component: NotFound,
  },
  {
    path: '/',
    component: Home,
    children: [
      {
        path: '',
        component: () => import('./pages/app/sections/Dashboard.vue'),
      },
      {
        path: 'projects/:pathMatch(.*)*',
        component: () => import('./pages/app/sections/Projects.vue'),
      },
      {
        path: 'settings',
        component: () => import('./pages/app/sections/Settings.vue'),
      },
    ],
  },
  {
    path: '/@:ownerName/:projectName',
    component: () => import('./pages/app/Editor.vue'),
  },
  {
    path: '/about',
    component: About,
  },
  {
    path: '/login',
    component: Login,
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes,
})

createApp(App)
  .use(router)
  .use(createPinia())
  .mount('#app')
