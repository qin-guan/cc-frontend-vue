import _ky from 'ky'

import type { Regions } from '../config'

export const ky = _ky.create({
  prefixUrl: import.meta.env.VITE_GLOBAL_API_URL,
  credentials: 'include',
})

export const regionalKy = (region: Regions) => _ky.create({
  prefixUrl: import.meta.env.VITE_REGIONAL_API_URL.replace('%s', region.toLowerCase()),
  credentials: 'include',
})
